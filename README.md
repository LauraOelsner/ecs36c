## ECS 36C - Data Structures

This repository contains my work done for homework (hw) assignments 1-3. The professor provided starter code, and students were tasked with completing the implementation. 

Each folder contains data (.dat), .h, and .cc files, as well as a PDF report.

* HW #1 is a demonstration of Big-O theory, testing the times for a linear and for a binary search of a vector.
* HW #2 demonstrates stacks and queues. 
	- Postfix Calculator: a stack is used to implement a postfix calculator.
	- Luggage Handling: and queue and a stack are used to determine the order luggage comes out of the airplane. Some luggage is handled in a first in, first out procedure, and some is handled by a last in, first out procedure. 
* HW #3 demonstrates binary trees, recursion, and testing. My work included: 
	- reconstructing a binary tree from data in a file 
	- finding the kth smallest key in a tree
	- testing that the tree was properly reconstructed and that outputs are as expected