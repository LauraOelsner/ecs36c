// ECS 36c -- HW 1
// Laura Oelsner
// vector_search.cc

#include <algorithm>
#include <chrono> // NOLINT (build/c++11)
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

class Timer {
 public:
  Timer() : start_(std::chrono::high_resolution_clock::now()) {}

  void Reset() {
    start_ = std::chrono::high_resolution_clock::now();
  }

  double GetDeltaTime() {
    std::chrono::high_resolution_clock::time_point now;
    now = std::chrono::high_resolution_clock::now();
    double delta_time = std::chrono::duration<double,
    std::micro> (now - start_).count();
    return delta_time;
  }

 private:
  std::chrono::high_resolution_clock::time_point start_;
};

class VectorXY {
 public:
  VectorXY(int x, int y) : y_(y), magnitude_(std::sqrt(x * x + y * y)) {}

  bool operator<(const VectorXY xy) const {
    if (magnitude_ != xy.magnitude_) {
      return (magnitude_ < xy.magnitude_);
    } else {
      return y_ < xy.y_;
    }
  }

  double GetMagnitude() const { return magnitude_; }
  void SetMagnitude(int new_magnitude) {magnitude_ = new_magnitude; }

 private:
  // int x_;
  int y_;
  double magnitude_;
};

bool BinarySearch(std::vector<VectorXY> &vec, int value) {
  std::size_t low_index = 0;
  std::size_t high_index = vec.size() - 1;

  // check if value is less/greater than the minimum/maximum
  if (value < vec.at(low_index).GetMagnitude()
    || value > vec.at(high_index).GetMagnitude()) {
    return false;
  }

  bool keep_searching = true;
  while (keep_searching) {
    std::size_t middle_index = low_index
    + (high_index - low_index) / 2;
    if (low_index == middle_index) {
      if (value == vec.at(middle_index).GetMagnitude() ||
        value == vec.at(high_index).GetMagnitude()) {
        return true;
      } else {
        keep_searching = false;
      }
    }
    if (value > vec.at(middle_index).GetMagnitude()) {
      low_index = middle_index;
    } else {
      high_index = middle_index;
    }
  }
  return false;
}

int main(int argc, char *argv[]) {
  if (4 != argc) {
    std::cerr << "Usage: " << argv[0]
    << " <vector_file.dat> <magnitude_file.dat> <result_file.dat>";
    return 0;
  }

  std::string vector_file, magnitude_file, result_file;
  vector_file = argv[1];
  magnitude_file = argv[2];
  result_file = argv[3];

  // get data from vector_file
  // open file
  std::ifstream in(vector_file);
  if (!in.good()) {
    std::cerr << "Error: cannot open file " << vector_file << std::endl;
    return 0;
  }

  // store values in a vector
  std::vector<VectorXY> vector_xy;
  while (in.peek() != EOF) {
    int x, y;
    in >> x >> y;
    vector_xy.push_back(VectorXY(x, y));
  }
  in.close();

  // get data from magnitude_file
  // open file
  in.open(magnitude_file);
  if (!in.good()) {
    std::cerr << "Error: cannot open file " << magnitude_file << std::endl;
    return 0;
  }

  // store values in a vector
  std::vector<int> vector_magnitudes;
  while (in.peek() != EOF) {
    int magnitude;
    in >> magnitude;
    vector_magnitudes.push_back(magnitude);
  }
  in.close();

  std::ofstream out;
  out.open(result_file, std::ios_base::app);
  if (!out.good()) {
    std::cerr << "Error: cannot open file " << result_file << std::endl;
    return 0;
  }

  Timer searching_time;
  bool showMenu = true;
  std::cout << "Choice of search method ([l]inear, [b]inary)?\n";
  while (showMenu) {
    char choice;
    std::cin >> choice;

    searching_time.Reset();
    switch (choice) {
      case 'l': {
        showMenu = false;

        // cast vector magnitudes from double to int type
        for (VectorXY &xy : vector_xy) {
          xy.SetMagnitude(static_cast<int>(xy.GetMagnitude()));
        }
        // linear search
        int count = 0;
        for (int magnitude : vector_magnitudes) {
          for (VectorXY xy : vector_xy) {
            if (magnitude == static_cast<int>(xy.GetMagnitude())) {
              ++count;
              break;
            }
          }
        }

        out << count;
        out.close();

        break;
      }
      case 'b': {
        showMenu = false;
        // sort list of vectors by magnitude, then by y-value
        std::sort(vector_xy.begin(), vector_xy.end());

        // cast copies of the vector magnitudes from double to int type
        for (VectorXY &xy : vector_xy) {
          xy.SetMagnitude(static_cast<int>(xy.GetMagnitude()));
        }

        // binary search
        int count = 0;
        for (int magnitude : vector_magnitudes) {
          bool found = BinarySearch(vector_xy, magnitude);
          if (found) {
            ++count;
          }
        }

        out << count;
        out.close();

        break;
      }
      default:
        std::cerr << "Incorrect choice\n";
        break;
    }
  }

  std::cout << "CPU time: "
    << searching_time.GetDeltaTime() << " microseconds\n";

  return 0;
}
