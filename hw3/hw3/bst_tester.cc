#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

#include "bst.h"

int main() {
  std::vector<int> keys1{51, 43, 93, 18, 42, 99, 54, 2, 74};
  // test on sorted values inserted into tree
  std::vector<int> keys2{1, 2, 3, 4, 5, 6, 7, 8};
  // test on negative keys
  std::vector<int> keys3{-1, -2, -3, -4, -5, -6, -7, -8};

  std::vector<std::vector<int>> keys_vector = {keys1, keys2, keys3};
  int round = 1;
  for (std::vector<int> keys : keys_vector) {
    BST<int> bst;

    try {
      bst.ceil(42);
    } catch (std::exception &e) {
      std::cout << "Expected exception: " << e.what() << std::endl;
    }

    for (auto i : keys)
      bst.insert(i);

    bst.print();

    /* Test ceil */
    std::cout << "ceil(-10) = " << bst.ceil(-10) << "\n";
    try {
      int c54 = bst.ceil(54);
      std::cout << "ceil(54) = " << c54 << "\n";
      std::cout << "ceil(55) = " << bst.ceil(55) << "\n";
    } catch (std::exception &e) {
      std::cout << "Expected exception on round 2 & 3: "
        << e.what() << std::endl;
    }
    try {  // test ceil value greater than all values in tree
      bst.ceil(100);
    } catch (std::exception &e) {
      std::cout << "Expected exception: " << e.what() << std::endl;
    }

    /* Test floor */
    std::cout << "floor(54) = " << bst.floor(54) << "\n";
    std::cout << "floor(55) = " << bst.floor(55) << "\n";
    try {  // floor value smaller than all values in tree
      int f1 = bst.floor(1);
      std::cout << "floor(1) = " << f1 << "\n";
    } catch (std::exception &e) {
      std::cout << "Expected exception: " << e.what() << std::endl;
    }

    /* Test kth_small */
    std::cout << "kth(1) = " << bst.kth_small(1) << "\n";
    std::cout << "kth(5) = " << bst.kth_small(5) << "\n";

    // Test for finding kth small on right side of tree
    std::cout << "kth(7) = " << bst.kth_small(7) << "\n";
    try {  // Test invalid input for k (negative number)
      bst.kth_small(-3);
    } catch (std::exception &e) {
      std::cout << "Expected exception: " << e.what() << std::endl;
    }
    std::cout << "= = = = = = = = = End round "
      << round << " = = = = = = = = = = =\n\n";
    ++round;
  }

  return 0;
}
