#ifndef BST_H_
#define BST_H_

#include <algorithm>
#include <iostream>
#include <memory>
#include <queue>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>

/*
 * Class definition
 */
template <typename T>
class BST {
 public:
  /* Return floor key in tree */
  const T& floor(const T &key);
  /* Return ceil key in tree */
  const T& ceil(const T &key);
  /* Return k-th smallest key in tree */
  const T& kth_small(const int kth);


  /* Return whether @key is found in tree */
  bool contains(const T& key);

  /* Return max key in tree */
  const T& max();
  /* Return min key in tree */
  const T& min();

  /* Insert @key in tree */
  void insert(const T &key);
  /* Remove @key from tree */
  void remove(const T &key);

  /* Print tree in-order */
  void print();

 private:
  struct Node{
    T key;
    std::unique_ptr<Node> left;
    std::unique_ptr<Node> right;
  };
  std::unique_ptr<Node> root;

  /* Useful recursive helper methods */
  void insert(std::unique_ptr<Node> &n, const T &key);
  void remove(std::unique_ptr<Node> &n, const T &key);
  void print(Node *n, int level);

  void queueTree(Node* n, std::queue<T> & left_queue);
};

/*
 * Implementation
 */
template <typename T>
const T& BST<T>::floor(const T &key) {
  /* Return the greatest value <= key */
  bool found = false;
  Node* n = root.get();
  if (!n) {
    throw std::runtime_error("Empty tree");
  }
  Node* closest_node = nullptr;
  while (!found) {
    if (!n) {
      if (!closest_node) {
        std::string error_str = "Cannot find floor for key ";
        error_str.append(std::to_string(key));
        throw std::domain_error(error_str);
      }
      return closest_node->key;
    } else if (n->key == key) {
      return key;

    } else if (n->key < key) {
      if (!closest_node || (key - closest_node->key > key - n->key)) {
        closest_node = n;
      }
      n = n->right.get();

    } else if (n->key > key) {
      n = n->left.get();
    }
  }
  return closest_node->key;
}

template <typename T>
const T& BST<T>::ceil(const T &key) {
  /* Return the smallest value >= key */
  bool found = false;
  Node* n = root.get();
  if (!n) {
    throw std::runtime_error("Empty tree");
  }
  Node* closest_node = nullptr;
  while (!found) {
    if (!n) {
      if (!closest_node) {
        std::string error_str = "Cannot find ceil for key ";
        error_str.append(std::to_string(key));
        throw std::domain_error(error_str);
      }
      return closest_node->key;
    } else if (n->key == key) {
      return key;

    } else if (n->key < key) {
      n = n->right.get();

    } else if (n->key > key) {
      if (!closest_node || closest_node->key - key > n->key - key) {
        closest_node = n;
      }
      n = n->left.get();
    }
  }
  return closest_node->key;
}

template <typename T>
const T& BST<T>::kth_small(const int kth) {
  /* Your implementation */
  if (kth <= 0) {
    std::stringstream ss;
    ss << "Invalid input (k = " << kth << "). k must be positive";
    throw std::runtime_error(ss.str());
  }
  std::queue<T> tree_queue;
  queueTree(root.get(), tree_queue);
  int num_pops = 0;
  while (num_pops < kth-1) {
    if (tree_queue.empty()) {
      break;
    }
    tree_queue.pop();
    ++num_pops;
  }
  if (num_pops == kth - 1) {
    return tree_queue.front();
  }

  // else, continue search in the right subtree for the kth smallest
  queueTree(root->right.get(), tree_queue);

  while (num_pops < kth-1) {
    if (tree_queue.empty()) {
      break;
    }
    tree_queue.pop();
    ++num_pops;
  }
  if (num_pops != kth - 1) {
    // there are less than k total elements in the tree
    std::string error_str = "Cannot find ";
    error_str.append(std::to_string(kth));
    error_str += "th element";
    throw std::domain_error(error_str);
  }
  return tree_queue.front();
}


template <typename T>
void BST<T>::queueTree(Node* n, std::queue<T> & tree_queue) {
  if (!n) {
    return;
  }
  queueTree(n->left.get(), tree_queue);
  tree_queue.push(n->key);
  queueTree(n->right.get(), tree_queue);
}


/*
 * @@@ Code below should not be modified @@@
 */
template <typename T>
bool BST<T>::contains(const T &key) {
  Node *n = root.get();

  while (n) {
    if (key == n->key)
      return true;

    if (key < n->key)
      n = n->left.get();
    else
      n = n->right.get();
  }

  return false;
}

template <typename T>
const T& BST<T>::max(void) {
  if (!root) throw std::runtime_error("Empty tree");
  Node *n = root.get();
  while (n->right) n = n->right.get();
  return n->key;
}

template <typename T>
const T& BST<T>::min(void) {
  if (!root) throw std::runtime_error("Empty tree");
  Node *n = root.get();
  while (n->left) n = n->left.get();
  return n->key;
}

template <typename T>
void BST<T>::insert(const T &key) {
  insert(root, key);
}

template <typename T>
void BST<T>::insert(std::unique_ptr<Node> &n, const T &key) {
  if (!n)
    n = std::unique_ptr<Node>(new Node{key});
  else if (key < n->key)
    insert(n->left, key);
  else if (key > n->key)
    insert(n->right, key);
  else
    std::cerr << "Key " << key << " already inserted!\n";
}

template <typename T>
void BST<T>::remove(const T &key) {
  remove(root, key);
}

template <typename T>
void BST<T>::remove(std::unique_ptr<Node> &n, const T &key) {
  /* Key not found */
  if (!n) return;

  if (key < n->key) {
    remove(n->left, key);
  } else if (key > n->key) {
    remove(n->right, key);
  } else {
    /* Found node */
    if (n->left && n->right) {
      /* Two children: replace with min node in right subtree */
      n->key = min(n->right.get())->key;
      remove(n->right, n->key);
    } else {
      /* Replace with only child or with nullptr */
      n = std::move((n->left) ? n->left : n->right);
    }
  }
}

template <typename T>
void BST<T>::print() {
  if (!root) return;
  print(root.get(), 1);
  std::cout << std::endl;
}

template <typename T>
void BST<T>::print(Node *n, int level) {
  if (!n) return;

  print(n->left.get(), level + 1);
  std::cout << n->key
      << " (" << level << ") ";
  print(n->right.get(), level + 1);
}

#endif /* BST_H_ */
