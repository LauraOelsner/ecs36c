//
//  main.cpp
//  hw3
//
//  Created by Laura Oelsner on 10/28/18.
//  Copyright © 2018 Laura Oelsner. All rights reserved.
//

#include "binary_tree.h"
#include "deconstructed_binary_tree.h"

#include <iostream>

int main(int argc, const char * argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <input_file>";
  }

  std::ifstream in_file(argv[1]);
  if (!in_file.good()) {
    std::cerr << "error: file didn't open";
  }

  DeconstructedBinaryTree<int> file_tree;
  bool valid = file_tree.ReadTreeData(in_file);

  if (!valid) {
    return 1;
  }

  BinaryTree<int> bin_tree(file_tree);

  bin_tree.PreorderPrint();
  bin_tree.InorderPrint();
  return 0;
}
