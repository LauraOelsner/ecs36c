//
//  deconstructed_binary_tree.h
//  hw3
//
//  Created by Laura Oelsner on 10/29/18.
//  Copyright © 2018 Laura Oelsner. All rights reserved.
//

#ifndef deconstructed_binary_tree_h
#define deconstructed_binary_tree_h

#include "binary_tree.h"

#include <fstream>
#include <iostream>
#include <sstream>

template <typename T>
class DeconstructedBinaryTree {
 public:
  std::stringstream _structure;
  int _real_leaves;
  std::stringstream _data;

  bool ReadTreeData(std::ifstream &in_file);
};

template <typename T>
bool DeconstructedBinaryTree<T>::ReadTreeData(std::ifstream &in_file) {
  if (in_file.peek() == EOF) {
    std::cerr << "Error: cannot read structure line1";
    return false;
  }

  while (in_file.peek() != '\n') {
    bool leaf;
    if (in_file >> leaf) {
      _structure << " " << leaf;
      if (leaf == true) {
        ++_real_leaves;
      }
    } else {
      std::cerr << "Error: structure line is invalid2";
      return false;
      break;
    }
  }

  T item;
  in_file >> item;
  if (in_file.eof()) {
    std::cerr << "Error: cannot read data line3";
    return false;
  }
  if (!in_file) {
    std::cerr << "Error: data line is invalid3.5";
    return false;
  }

  _data << " " << item;
  for (int i = 0; i < _real_leaves - 1; ++i) {
    if (in_file >> item) {
      _data << " " << item;
    } else {
      std::cerr << "Error: data line is invalid4";
      return false;
    }
  }
  in_file >> item;
  if (in_file) {
    std::cerr << "Error: data line is invalid5";
    return false;
  }
  return true;
}

#endif /* deconstructed_binary_tree_h */
