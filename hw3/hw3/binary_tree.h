#ifndef BINARY_TREE_H_
#define BINARY_TREE_H_

#include "deconstructed_binary_tree.h"

#include <iostream>
#include <memory>
#include <sstream>

/*
 * Class definition
 */
template <typename T>
class BinaryTree {
 public:
  struct Node{
    T _item;
    std::unique_ptr<Node> _left;
    std::unique_ptr<Node> _right;
  };
  std::unique_ptr<Node> _root;

  void DecodeBinaryTree(std::unique_ptr<Node> & n,
                        DeconstructedBinaryTree<T>& dTree);
  BinaryTree(DeconstructedBinaryTree<T>& dTree);

  void PreorderPrint();
  void InorderPrint();
  void PostorderPrint();

 private:
  void PreorderConstruct(Node *n,
                         std::stringstream s_struct,
                         std::stringstream s_data);
  void PreorderRecur(Node *n);
  void InorderRecur(Node *n);
};

/*
 * Implementation
 */
template <typename T>
void BinaryTree<T>::DecodeBinaryTree(std::unique_ptr<Node> & n,
                                     DeconstructedBinaryTree<T>& dTree) {
  bool real_leaf;
  dTree._structure >> real_leaf;
  if (!real_leaf) {
    n = nullptr;
  } else {
    n = std::unique_ptr<Node>(new Node);
    T data;
    dTree._data >> data;
    n->_item = data;
    DecodeBinaryTree(n->_left, dTree);
    DecodeBinaryTree(n->_right, dTree);
  }
}

template <typename T>
BinaryTree<T>::BinaryTree(DeconstructedBinaryTree<T>& dTree) {
  _root = std::unique_ptr<Node>(new Node);
  DecodeBinaryTree(_root, dTree);
}

template <typename T>
void BinaryTree<T>::PreorderPrint() {
  std::cout << "Preorder: ";
  PreorderRecur(_root.get());
  std::cout << std::endl;
}

template <typename T>
void BinaryTree<T>::InorderPrint() {
  std::cout << "Inorder: ";
  InorderRecur(_root.get());
  std::cout << std::endl;
}

template <typename T>
void BinaryTree<T>::PreorderRecur(Node *n) {
  if (!n) return;
  std::cout << n->_item << ' ';
  PreorderRecur(n->_left.get());
  PreorderRecur(n->_right.get());
}

template <typename T>
void BinaryTree<T>::InorderRecur(Node *n) {
  if (!n) return;
  InorderRecur(n->_left.get());
  std::cout << n->_item << ' ';
  InorderRecur(n->_right.get());
}

#endif /* BINARY_TREE_H_ */
