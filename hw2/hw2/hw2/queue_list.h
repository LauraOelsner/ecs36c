// using instructor provided queue_list.h

#ifndef QUEUE_LIST_H_
#define QUEUE_LIST_H_

#include <list>
#include <stdexcept>


template <typename T>
class Queue {
 public:
  // Return number of items in queue
  unsigned int Size();
  // Return front of queue
  T& Front();
  // Return back of queue
  T& Back();
  // Remove front of queue
  void Pop();
  // Push item to back of queue
  void Push(const T &item);

 private:
  std::list<T> items;
};

template <typename T>
unsigned int Queue<T>::Size() {
  return items.size();
}

template <typename T>
T& Queue<T>::Front() {
  if (!Size())
    throw std::underflow_error("Empty queue!");
  return items.front();
}

template <typename T>
T& Queue<T>::Back() {
  if (!Size())
    throw std::underflow_error("Empty queue!");
  return items.back();
}

template <typename T>
void Queue<T>::Pop() {
  if (!Size())
    throw std::underflow_error("Empty queue!");
  items.pop_front();
}

template <typename T>
void Queue<T>::Push(const T &item) {
  items.push_back(item);
}

#endif  // QUEUE_LIST_H_
