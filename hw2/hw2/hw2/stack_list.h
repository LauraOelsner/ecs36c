// using instructor provided stack_list.h

#ifndef STACK_LIST_H_
#define STACK_LIST_H_

#include <forward_list>
#include <stdexcept>

template <typename T>
class Stack {
 public:
  unsigned int Size(); // Return number of items in stack
  T& Top(); // Return top of stack
  void Pop(); // Remove top of stack
  void Push(const T &item); // Push item to top of stack

 private:
  unsigned int cur_size = 0;
  std::forward_list<T> items;
};

template <typename T>
unsigned int Stack<T>::Size() {
  return cur_size;
}

template <typename T>
T& Stack<T>::Top(void) {
  if (!Size())
    throw std::underflow_error("Empty stack!");
  return items.front();
}

template <typename T>
void Stack<T>::Pop() {
  if (!Size())
    throw std::underflow_error("Empty stack!");
  items.pop_front();
  cur_size--;
}

template <typename T>
void Stack<T>::Push(const T &item) {
  items.push_front(item);
  cur_size++;
}

#endif  // STACK_LIST_H_
