//
//  postfix_eval.cpp
//  hw2
//
//  Created by Laura Oelsner on 10/15/18.
//  Copyright © 2018 Laura Oelsner. All rights reserved.
//

#include "stack_list.h"

#include <iostream>
#include <sstream>
#include <stdexcept>

class PostFixCalculator {
 public:
  enum NumOpEnum {
    number_enum, operand_enum
  };

  void Calculate(std::stringstream &s_in);
  static double Operate(double num1, double num2,
                        char operand, bool &valid_operand);
  bool NextInputValid(std::stringstream &s_in, NumOpEnum &next);
 private:
  Stack<double> values_stack_;
};

bool PostFixCalculator::NextInputValid(std::stringstream &s_in,
                                       NumOpEnum &next) {
  if (s_in.peek() == ' ') {
    s_in.ignore();
  }
  if (s_in.peek() == '+' || s_in.peek() == '-'
    || s_in.peek() =='*' || s_in.peek() == '/' || !isdigit(s_in.peek())) {
	  
    if (values_stack_.Size() < 2) {
      // next input needs to be a number
    std::cerr<< "Error: invalid expression";
      return false;
    } else {
      next = operand_enum;
      return true;
    }
  } else {
    if (isdigit(s_in.peek())) {
      // next input is a number
      next = number_enum;
      return true;
    }
  }
  return false;
}

void PostFixCalculator::Calculate(std::stringstream &s_in) {
  double answer = 0;
  bool invalid = false;
  for (int i = 0; i < 2; ++i) {
    double number;
    if (s_in >> number) {
      values_stack_.Push(number);
    } else {
      std::cerr << "Invalid expression";
      invalid = true;
    }
  }

  long long int current_position = s_in.tellg();
  s_in.seekg(0, std::ios::end);
  int str_end = s_in.tellg();
  int characters_left = str_end - current_position;
  bool keep_calculating = (characters_left > 0);
	
  while (keep_calculating) {
    s_in.seekg(current_position);
    NumOpEnum next_type;
    bool valid = NextInputValid(s_in, next_type);
    if (!valid) {
      std::cerr << "invalid input";
      invalid = true;
      break;
    }
    if (next_type == number_enum) {
      double number;
      s_in >> number;
      values_stack_.Push(number);
    } else {
      //s_in.ignore();
      char op;
      s_in >> op;
      //if (op=='+' || op =='-' || op=='*' || op=='/') {
        double num1, num2;
        num2 = values_stack_.Top();
        values_stack_.Pop();
        num1 = values_stack_.Top();
        values_stack_.Pop();

    bool valid_operand;
        answer = Operate(num1, num2, op, valid_operand);
    if (valid_operand) {
      values_stack_.Push(answer);
    } else {
      if (s_in.peek() != EOF) {
        char temp;
        s_in >> temp;
        //s_in >> op;
        std::cerr << temp;
      }
      std::cerr << "'";
      keep_calculating = false;
      break;
    }
      //}
    }

    current_position = s_in.tellg();
    s_in.seekg(0, std::ios::end);
    str_end = s_in.tellg();
    characters_left = str_end - current_position;
    keep_calculating = (characters_left > 0);
  }

  if (values_stack_.Size() > 1) {
    invalid = true;
  }

  if (!invalid){
    answer = values_stack_.Top();
    std::cout << answer << std::endl;
  }
}

double PostFixCalculator::Operate(double num1, double num2, char operand, bool &valid_operand) {
  valid_operand = true;
  double answer;
  switch (operand) {
    case '+':
      answer = num1 + num2;
      break;
    case '-':
      answer = num1 - num2;
      break;
    case '*':
      answer = num1*num2;
      break;
    case '/':
      answer = num1/num2;
      break;
    default:
    std::cerr << "Error: unknown symbol '" << operand;
      answer = -99;
    valid_operand = false;
      break;
  }
  return answer;
}

int main(int argc, const char * argv[]) {
  while (std::cin.peek() != EOF) {
    std::string line;
    getline(std::cin, line);
    std::stringstream s_in(line);

    PostFixCalculator post_fix_calc;
    post_fix_calc.Calculate(s_in);
  }

  std::cout << "Bye!";

  return 0;
}
