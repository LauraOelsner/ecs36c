////
////  main.cpp
////  hw2
////
////  Created by Laura Oelsner on 10/12/18.
////  Copyright © 2018 Laura Oelsner. All rights reserved.
////
//
//#include "stack_list.h"
//
////#include <forward_list>
//#include <iostream>
//#include <sstream>
//#include <string>
//
//class PostFixStack {
//public:
//	enum NumOpEnum {
//		number_enum, operand_enum
//	};
//
//	PostFixStack() = default;
//	void Push(double num);
//	void Calculate(std::stringstream &s_in);
//	double Top() const;
//	void Pop();
//	static double Operate(double num1, double num2, char operand);
//	bool NextInputValid(std::stringstream &s_in, NumOpEnum &next);
//
//private:
//	std::forward_list<double> num_stack_;
//	std::size_t current_size_ = 0;
//};
//
//void PostFixStack::Push(double num) {
//	num_stack_.push_front(num);
//	++current_size_;
//}
//
//double PostFixStack::Top() const {
//	return num_stack_.front();
//}
//
//void PostFixStack::Pop() {
//	num_stack_.pop_front();
//	--current_size_;
//}
//
//bool PostFixStack::NextInputValid(std::stringstream &s_in, NumOpEnum &next) {
//	if (s_in.peek() == ' ') {
//		s_in.ignore();
//	}
//	if (s_in.peek() == '+' || s_in.peek() == '-'
//		|| s_in.peek() =='*' || s_in.peek() == '/') {
//
//		if (current_size_ < 2) {
//			// next input needs to be a number
//			return false;
//		} else {
//			next = operand_enum;
//			return true;
//		}
//	} else {
//		if (isdigit(s_in.peek())) {
//			// next input is a number
//			next = number_enum;
//			return true;
//		}
//	}
//	return false;
//}
//
//void PostFixStack::Calculate(std::stringstream &s_in) {
//	double answer = 0;
//	bool invalid = false;
//	for (int i = 0; i<2; ++i) {
//		double number;
//		if (s_in>>number) {
//			Push(number);
//		} else {
//			std::cout << "Invalid expression";
//			invalid = true;
//		}
//	}
//
//	long long int current_position = s_in.tellg();
//	s_in.seekg(0, std::ios::end);
//	long long int str_end = s_in.tellg();
//	long long int characters_left = str_end - current_position;
//	bool keep_calculating = (characters_left > 0);
//
//	while (keep_calculating) {
//		s_in.seekg(current_position);
//		NumOpEnum next_type;
//		bool valid = NextInputValid(s_in, next_type);
//		if (!valid) {
//			std::cout << "invalid input";
//			invalid = true;
//			break;
//		}
//		if (next_type == number_enum) {
//			double number;
//			s_in>>number;
//			Push(number);
//		} else {
//			//s_in.ignore();
//			char op;
//			s_in >> op;
//			if (op=='+' || op =='-' || op=='*' || op=='/') {
//				double num1, num2;
//				num2 = Top();
//				Pop();
//				num1 = Top();
//				Pop();
//
//				answer = Operate(num1, num2, op);
//				Push(answer);
//			}
//		}
//
//		current_position = s_in.tellg();
//		s_in.seekg(0, std::ios::end);
//		str_end = s_in.tellg();
//		characters_left = str_end - current_position;
//		keep_calculating = (characters_left > 0);
//	}
//
//	if (current_size_ > 1) {
//		invalid = true;
//	}
//
//	if (!invalid){
//		answer = Top();
//		std::cout << answer << std::endl;
//	}
//}
//
//double PostFixStack::Operate(double num1, double num2, char operand) {
//	double answer;
//	switch (operand) {
//		case '+':
//			answer = num1 + num2;
//			break;
//		case '-':
//			answer = num1 - num2;
//			break;
//		case '*':
//			answer = num1*num2;
//			break;
//		case '/':
//			answer = num1/num2;
//			break;
//		default:
//			std::cout << "invalid oeprand";
//			answer = -99;
//			break;
//	}
//	return answer;
//}
//
//int main(int argc, const char * argv[]) {
//	while (std::cin.peek() != EOF) {
//		std::string line;
//		getline(std::cin, line);
//		std::cout << "it was collected\n";
//		std::stringstream s_in(line);
//
//		PostFixStack post_fix;
//		post_fix.Calculate(s_in);
//	}
//
//	std::cout << "Bye!";
////	class Stack {
////	public:
////		// Return number of items in stackunsigned
////		int Size();
////
////		// Return top of stack
////		T& Top();
////
////		// Remove top of stack
////		void Pop();
////
////		// Push item to top of stack
////		void Push(const T &item);
////		private:
////		unsigned int cur_size = 0;
////		std::forward_list<T> items;
////
////	};
////
////	template <typename T>unsigned int Stack<T>::Size() {return cur_size;}
////	template <typename T>T& Stack<T>::Top(void) {
////		if (!Size())throw std::underflow_error("Empty stack!");
////		return items.front();}
////	template <typename T>void Stack<T>::Pop() {
////		if (!Size())throw std::underflow_error("Empty stack!");
////		items.pop_front();cur_size--;}
////	template <typename T>void Stack<T>::Push(const T &item) {
////		items.push_front(item);cur_size++;}
//
//
//	return 0;
//}
