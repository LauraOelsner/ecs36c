//
//  luggage_handling.cpp
//  hw2
//
//  Created by Laura Oelsner on 10/15/18.
//  Copyright © 2018 Laura Oelsner. All rights reserved.
//

#include "stack_vector.h"
#include "queue_list.h"

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] <<" <input_file> <container_size>";
  }

  std::ifstream in(argv[1]);
  if (!in.good()) {
    std::cerr << "Error: cannot open file " << argv[1];
  }

  unsigned int max_bags_in_container = std::stoi(argv[2]);
  if ((max_bags_in_container < 1)
      || (static_cast<double>(max_bags_in_container)
      != std::stod(argv[2]))) {
    std::cerr << "Error: invalid container size";
  }

  Queue<int> container_of_bags;
  StackVector<Queue<int>> airplane_of_containers;

  // Load bags
  while (in.peek() != EOF) {
    int bag_id;
    in >> bag_id;
    if (container_of_bags.Size()< max_bags_in_container) {
      container_of_bags.Push(bag_id);
    }
  if (in.peek() == EOF || container_of_bags.Size() == max_bags_in_container) {
      airplane_of_containers.Push(container_of_bags);
      int last_bag_size = container_of_bags.Size();
      for (int i = 0; i < last_bag_size; ++i) {
        container_of_bags.Pop();
      }
    }
  }

  // Unload bags
  while (airplane_of_containers.Size() != 0) {
    container_of_bags = airplane_of_containers.Top();
    while (container_of_bags.Size() != 0) {
      int bag_id = container_of_bags.Front();
      std::cout <<bag_id << " ";
      container_of_bags.Pop();
    }
    airplane_of_containers.Pop();
  }

  return 0;
}
