// using instructor provided stack_vector.h

#ifndef STACK_VECTOR_H_
#define STACK_VECTOR_H_

#include <stdexcept>
#include <vector>

template <typename T>
class StackVector {
 public:
  // Return number of items in StackVector
  unsigned int Size();
  // Return top of StackVector
  T& Top();
  // Remove top of StackVector
  void Pop();
  // Push item to top of StackVector
  void Push(const T &item);

 private:
  std::vector<T> items;
};

template <typename T>
unsigned int StackVector<T>::Size() {
  return items.size();
}

template <typename T>
T& StackVector<T>::Top(void) {
  if (!Size())
    throw std::underflow_error("Empty StackVector!");
  return items.back();
}

template <typename T>
void StackVector<T>::Pop() {
  if (!Size())
    throw std::underflow_error("Empty StackVector!");
  items.pop_back();
}

template <typename T>
void StackVector<T>::Push(const T &item) {
  items.push_back(item);
}

#endif  // STACK_VECTOR_H_
